'use strict';
//Selecting Element
const player0EL = document.querySelector('.player--0');
const player1EL = document.querySelector('.player--1');
const score0El = document.querySelector('#score--0');
const score1El = document.querySelector('#score--1');
const current0EL = document.getElementById('current--0');
const current1EL = document.getElementById('current--1');
const diceEL = document.querySelector('.dice');
const btnNew = document.querySelector('.btn--new');
const btnRoll = document.querySelector('.btn--roll');
const btnHold = document.querySelector('.btn--hold');
const name0EL = document.querySelector('#name--0');
const name1EL = document.querySelector('#name--1');
let currentScore, activePlayer, score, playing;
//Starting Conditions
const init = function() {
    currentScore = 0;
    activePlayer = 0;
    score = [0, 0];
    playing = true;
    score0El.textContent = 0;
    score1El.textContent = 0;

    current0EL.textContent = 0;
    current1EL.textContent = 0;

    player0EL.classList.remove('player--winner');
    player1EL.classList.remove('player--winner');

    player0EL.classList.add('player--active');
    player1EL.classList.remove('player--active');

    name0EL.textContent = 'Player 1';
    name1EL.textContent = 'Player 2';

    diceEL.classList.add('hidden');
};
init();
//Rolling Btn Funtionatity
const switchScore = function() {
    document.getElementById(`current--${activePlayer}`).textContent = 0;
    currentScore = 0;
    activePlayer = activePlayer === 0 ? 1 : 0;
    player0EL.classList.toggle('player--active');
    player1EL.classList.toggle('player--active');
};
btnRoll.addEventListener('click', function() {
    if (playing) {
        //1.Generating a random dice roll

        const dice = Math.trunc(Math.random() * 6) + 1;
        //console.log(dice);

        //2.Display Dice Number
        diceEL.classList.remove('hidden');
        diceEL.src = `dice-${dice}.png`;

        //3.Check roll 1:if true then play next player

        if (dice !== 1) {
            currentScore += dice;
            document.getElementById(
                `current--${activePlayer}`
            ).textContent = currentScore;
            //add dice to current score
        } else {
            //Switch to other Player

            switchScore();
        }
    }
});
btnHold.addEventListener('click', function() {
    if (playing) {
        //1.Add currentScore to ActivePlayer
        score[activePlayer] += currentScore;

        document.getElementById(`score--${activePlayer}`).textContent =
            score[activePlayer];
        // 2. Check  if player 's score is >=100
        if (score[activePlayer] >= 20) {
            playing = false;
            //Finish the game
            document
                .querySelector(`.player--${activePlayer}`)
                .classList.add('player--winner');
            document
                .querySelector(`.player--${activePlayer}`)
                .classList.remove('player--active');

            diceEL.classList.add('hidden');
            document.getElementById(`current--${activePlayer}`).textContent = 0;
            document.querySelector('#name--' + activePlayer).textContent =
                'Winner 🏆';
        } else {
            //Switch to the next player

            switchScore();
        }
    }
});
btnNew.addEventListener('click', init);